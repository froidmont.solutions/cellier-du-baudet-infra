data "hetznerdns_zone" "cellier_zone" {
  name = "epicerieducellier.be"
}

resource "hetznerdns_record" "mx" {
  zone_id = data.hetznerdns_zone.cellier_zone.id
  name    = "@"
  value   = "10 www374.your-server.de."
  type    = "MX"
  ttl     = 600
}

resource "hetznerdns_record" "dkim" {
  zone_id = data.hetznerdns_zone.cellier_zone.id
  name    = "default2302._domainkey"
  value   = "\"v=DKIM1; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApEbFUVBSIC3dEMwzwociH\" \"/HA9/Ypx3slnjnJ31dKtF2xpV+9KxbItZVuPh6Egs+OLfXruUx24bUMfFSMKph8PdwmT19PYAnuv\" \"uzUMLGWrxWVh+dlbwE/NpoVBeGn3quB3iP3qLLDJXhmrRnJMG77q3OHnm6UpU2CZw6fD+6nZ4DO+\" \"nDrN2U/uDR30zD2SQ+yKDcfx//D7crUnU1bOcTi4/xkiJpHJhXKtzgH36UFeYhXrdSK4oZRvMS/u\" \"A/6xPfVHc4WWRLlZQ9FuIh1uoWgXRlKLwOTwkxs0QQQgcXp/M5PS+yOMZgXskh7oq5TSXCv2ic4o\" \"ubqRim7TCbz5BbsAQIDAQAB\" "
  type    = "TXT"
  ttl     = 600
}

resource "hetznerdns_record" "spf" {
  zone_id = data.hetznerdns_zone.cellier_zone.id
  name    = "epicerieducellier.be."
  value   = "\"v=spf1 a mx ip4:78.46.152.50 ~all\""
  type    = "TXT"
  ttl     = 600
}

resource "hetznerdns_record" "dmarc" {
  zone_id = data.hetznerdns_zone.cellier_zone.id
  name    = "_dmarc.epicerieducellier.be."
  value   = "\"v=DMARC1;p=none;sp=none;pct=50;adkim=r;aspf=r;\""
  type    = "TXT"
  ttl     = 600
}

resource "hetznerdns_record" "backend1_a" {
  zone_id = data.hetznerdns_zone.cellier_zone.id
  name    = "backend1"
  value   = hcloud_server.backend1.ipv4_address
  type    = "A"
  ttl     = 600
}

resource "hetznerdns_record" "cloud_a" {
  zone_id = data.hetznerdns_zone.cellier_zone.id
  name    = "cloud"
  value   = hcloud_server.backend1.ipv4_address
  type    = "A"
  ttl     = 600
}

resource "hetznerdns_record" "wiki_a" {
  zone_id = data.hetznerdns_zone.cellier_zone.id
  name    = "wiki"
  value   = hcloud_server.backend1.ipv4_address
  type    = "A"
  ttl     = 600
}

resource "hetznerdns_record" "gestion_a" {
  zone_id = data.hetznerdns_zone.cellier_zone.id
  name    = "gestion"
  value   = hcloud_server.backend1.ipv4_address
  type    = "A"
  ttl     = 600
}

resource "hetznerdns_record" "membres_a" {
  zone_id = data.hetznerdns_zone.cellier_zone.id
  name    = "membres"
  value   = hcloud_server.backend1.ipv4_address
  type    = "A"
  ttl     = 600
}

resource "hetznerdns_record" "www_a" {
  zone_id = data.hetznerdns_zone.cellier_zone.id
  name    = "www"
  value   = hcloud_server.backend1.ipv4_address
  type    = "A"
  ttl     = 600
}

resource "hetznerdns_record" "a" {
  zone_id = data.hetznerdns_zone.cellier_zone.id
  name    = "@"
  value   = hcloud_server.backend1.ipv4_address
  type    = "A"
  ttl     = 600
}
