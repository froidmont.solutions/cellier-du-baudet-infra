data "hcloud_image" "nixos_stable" {
  with_selector = "nixos=21.11"
}

resource "hcloud_server" "backend1" {
  name        = "backend1"
  image       = data.hcloud_image.nixos_stable.id
  server_type = "cpx11"
  ssh_keys = [
    hcloud_ssh_key.froidmpa-desktop.id,
    hcloud_ssh_key.froidmpa-laptop.id
  ]
  keep_disk = true
  location  = "fsn1"

  labels = {
    type = "backend"
  }

  lifecycle {
    ignore_changes = [
      ssh_keys
    ]
  }
}
