{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    # required for old PHP versions
    phps.url = "github:fossar/nix-phps";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    sops-nix.url = "github:Mic92/sops-nix";
    sops-nix.inputs.nixpkgs.follows = "nixpkgs";
    deploy-rs.url = "github:serokell/deploy-rs";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      phps,
      nixpkgs-unstable,
      deploy-rs,
      sops-nix,
      ...
    }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      pkgs-unstable = nixpkgs-unstable.legacyPackages.x86_64-linux;
    in
    {
      devShells.x86_64-linux.default = pkgs.mkShell {
        sopsPGPKeyDirs = [
          "./keys/hosts"
          "./keys/users"
        ];

        nativeBuildInputs = [ (pkgs.callPackage sops-nix { }).sops-import-keys-hook ];

        buildInputs = with pkgs-unstable; [
          nixpkgs-fmt
          opentofu
          terraform-ls
          nodejs
          sops
          deploy-rs.packages.x86_64-linux.deploy-rs
        ];
      };

      nixosConfigurations = {
        backend1 = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            sops-nix.nixosModules.sops
            ./profiles/backend.nix
            {
              sops.defaultSopsFile = ./secrets.enc.yml;
              networking.hostName = "backend1";
              networking.domain = "epicerieducellier.be";
              nix.registry.nixpkgs.flake = nixpkgs;

              nixpkgs.config.packageOverrides = pkgs: {
                php74 = phps.packages.x86_64-linux.php74;
              };

              system.stateVersion = "22.11";
            }
          ];
        };
      };

      deploy.nodes =
        let
          createSystemProfile = configuration: {
            user = "root";
            sshUser = "root";
            path = deploy-rs.lib.x86_64-linux.activate.nixos configuration;
          };
        in
        {
          backend1 = {
            hostname = "backend1.epicerieducellier.be";
            profiles.system = createSystemProfile self.nixosConfigurations.backend1;
          };
        };

      checks = builtins.mapAttrs (system: deployLib: deployLib.deployChecks self.deploy) deploy-rs.lib;
    };
}
