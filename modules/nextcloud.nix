{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.custom.services.nextcloud;
in
{
  options.custom.services.nextcloud = {
    enable = mkEnableOption "nextcloud";

    dbPasswordFile = mkOption { type = types.path; };

    adminPasswordFile = mkOption { type = types.path; };
  };

  config = mkIf cfg.enable {
    services.nginx.virtualHosts."${config.services.nextcloud.hostName}" = {
      enableACME = true;
      forceSSL = true;
    };

    services.nextcloud = {
      enable = true;
      package = pkgs.nextcloud30;
      hostName = "cloud.${config.networking.domain}";
      https = true;
      configureRedis = true;
      home = "/nix/var/data/nextcloud";
      config = {
        dbtype = "pgsql";
        dbuser = "nextcloud";
        dbhost = "127.0.0.1";
        dbname = "nextcloud";
        dbpassFile = cfg.dbPasswordFile;
        adminpassFile = cfg.adminPasswordFile;
        adminuser = "admin";
      };

      settings = {
        maintenance_window_start = 1;
        overwriteprotocol = "https";
        default_phone_region = "BE";
      };

      phpOptions = {
        short_open_tag = "Off";
        expose_php = "Off";
        error_reporting = "E_ALL & ~E_DEPRECATED & ~E_STRICT";
        display_errors = "stderr";
        "opcache.enable_cli" = "1";
        "opcache.interned_strings_buffer" = "12";
        "opcache.max_accelerated_files" = "10000";
        "opcache.memory_consumption" = "128";
        "opcache.revalidate_freq" = "1";
        "opcache.fast_shutdown" = "1";
        "openssl.cafile" = "/etc/ssl/certs/ca-certificates.crt";
        catch_workers_output = "yes";
      };
    };
  };
}
