{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
let
  php = pkgs.php74;
  composer = php.buildComposerProject (finalAttrs: {

    pname = "composer";
    version = "2.2.18";

    src = pkgs.fetchFromGitHub {
      owner = "composer";
      repo = "composer";
      rev = finalAttrs.version;
      hash = "sha256-CUyIs8KdINDrJkJthrbd8+AhQNtikOYVD0NA3WmXTj4=";
    };

    nativeBuildInputs = [ pkgs.makeBinaryWrapper ];

    postInstall = ''
      wrapProgram $out/bin/composer \
        --prefix PATH : ${
          lib.makeBinPath (
            with pkgs;
            [
              _7zz
              cacert
              curl
              git
              unzip
              xz
            ]
          )
        }
    '';

    vendorHash = "sha256-Hx6B/xCToqi+CPILJaLGKCkfjCswjfRyyo6qZv5B//Q=";

    meta = {
      changelog = "https://github.com/composer/composer/releases/tag/${finalAttrs.version}";
      description = "Dependency Manager for PHP";
      homepage = "https://getcomposer.org/";
      license = lib.licenses.mit;
      mainProgram = "composer";
      maintainers = lib.teams.php.members;
    };
  });

  installDir = "/nix/var/www/volunteer-management";

  cfg = config.custom.services.volunteer-management;
in
{
  options.custom.services.volunteer-management = {
    enable = mkEnableOption "volunteer-management";
  };

  config = mkIf cfg.enable {

    environment.systemPackages = with pkgs; [
      php
      git
      composer
    ];

    # fr_FR is used by PHP to fromat dates
    i18n.supportedLocales = [
      "en_US.UTF-8/UTF-8"
      "fr_FR.UTF-8/UTF-8"
    ];

    services.nginx = {

      virtualHosts."gestion.${config.networking.domain}" = {
        forceSSL = true;
        enableACME = true;

        locations."/" = {
          extraConfig = ''
            rewrite ^/(.*)$ https://membres.${config.networking.domain}/$1 permanent;
          '';
        };
      };
      virtualHosts."membres.${config.networking.domain}" = {
        forceSSL = true;
        enableACME = true;

        root = "${installDir}/web";

        locations."/" = {
          extraConfig = ''
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            try_files $uri /app.php$is_args$args;
          '';
        };

        locations."~ ^/app\\.php(/|$)" = {
          extraConfig = ''
            fastcgi_pass unix:${config.services.phpfpm.pools.volunteer-management.socket};
            fastcgi_intercept_errors on;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include ${config.services.nginx.package}/conf/fastcgi.conf;
            fastcgi_param REMOTE_ADDR $http_x_real_ip;
            fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
            fastcgi_param DOCUMENT_ROOT $realpath_root;
            internal;
          '';
        };

        locations."~* ^/sw/(.*)/(qr|br)\\.png$" = {
          extraConfig = ''
            rewrite ^/sw/(.*)/(qr|br)\.png$ /app.php/sw/$1/$2.png last;
          '';
        };

        extraConfig = ''
          location ~ \.php$ {
            return 404;
          }
        '';
      };
    };

    services.phpfpm.pools.volunteer-management = {
      user = "nginx";
      settings = {
        pm = "dynamic";
        "listen.owner" = config.services.nginx.user;
        "pm.max_children" = 5;
        "pm.start_servers" = 2;
        "pm.min_spare_servers" = 1;
        "pm.max_spare_servers" = 3;
        "pm.max_requests" = 500;
      };
    };

    services.mysql = {
      enable = true;
      package = pkgs.mariadb_1011;
      initialDatabases = [ { name = "volunteer_management"; } ];
      ensureUsers = [
        {
          name = "volunteer_management";
          ensurePermissions = {
            "volunteer_management.*" = "ALL PRIVILEGES";
          };
        }
        {
          name = "root";
          ensurePermissions = {
            "*.*" = "ALL PRIVILEGES";
          };
        }
      ];
    };

    services.mysqlBackup = {
      enable = true;
      singleTransaction = true;
      databases = [ "volunteer_management" ];
      calendar = "00:00";
    };

    systemd.timers."generate-shifts" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "*-*-* 5:55:00";
        Unit = "generate-shifts.service";
      };
    };

    systemd.services."generate-shifts" = {
      script = ''
        # generate shifts in 27 days (same weekday as yesterday)
        ${php}/bin/php ${installDir}/bin/console app:shift:generate $(date -d "+27 days" +\%Y-\%m-\%d)
      '';
      serviceConfig = {
        Type = "oneshot";
        User = "nginx";
      };
    };

    systemd.timers."free-pre-booked-shifts" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "*-*-* 5:55:00";
        Unit = "free-pre-booked-shifts.service";
      };
    };

    systemd.services."free-pre-booked-shifts" = {
      script = ''
        # free pre-booked shifts
        ${php}/bin/php ${installDir}/bin/console app:shift:free $(date -d "+21 days" +\%Y-\%m-\%d)
      '';
      serviceConfig = {
        Type = "oneshot";
        User = "nginx";
      };
    };

    systemd.timers."send-shift-reminders" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "*-*-* 6:00:00";
        Unit = "send-shift-reminders.service";
      };
    };

    systemd.services."send-shift-reminders" = {
      script = ''
        # send reminder 2 days before shift
        ${php}/bin/php ${installDir}/bin/console app:shift:reminder $(date -d "+2 days" +\%Y-\%m-\%d)
      '';
      serviceConfig = {
        Type = "oneshot";
        User = "nginx";
      };
    };

    systemd.timers."cycle-start" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "*-*-* 6:05:00";
        Unit = "cycle-start.service";
      };
    };

    systemd.services."cycle-start" = {
      script = ''
        # execute routine for cycle_end/cycle_start, everyday
        ${php}/bin/php ${installDir}/bin/console app:user:cycle_start
      '';
      serviceConfig = {
        Type = "oneshot";
        User = "nginx";
      };
    };

    systemd.timers."send-alerts" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "*-*-* 10:00:00";
        Unit = "send-alerts.service";
      };
    };

    systemd.services."send-alerts" = {
      script = ''
        # send alert on shifts booking (low)
        ${php}/bin/php ${installDir}/bin/console app:shift:send_alerts $(date -d "+2 days" +\%Y-\%m-\%d) 1
      '';
      serviceConfig = {
        Type = "oneshot";
        User = "nginx";
      };
    };

    systemd.timers."send-code-reminders" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "*-*-* 21:45:00";
        Unit = "send-code-reminders.service";
      };
    };

    systemd.services."send-code-reminders" = {
      script = ''
        # send a reminder mail to the user who generate the last code but did not validate the change.
        ${php}/bin/php ${installDir}/bin/console app:code:verify_change --last_run 24
      '';
      serviceConfig = {
        Type = "oneshot";
        User = "nginx";
      };
    };
  };
}
