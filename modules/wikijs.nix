{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.custom.services.wiki-js;
in
{
  options.custom.services.wiki-js = {
    enable = mkEnableOption "wiki-js";

    envFile = mkOption { type = types.path; };
  };

  config = mkIf cfg.enable {
    services.wiki-js = {
      enable = true;
      settings = {
        db.type = "postgres";
        db.host = "127.0.0.1";
        db.db = "wikijs";
        db.user = "wikijs";
        db.pass = "$(DB_PASS)";
        theming.tocPosition = "right";
      };
      environmentFile = cfg.envFile;
    };

    services.nginx.virtualHosts."wiki.${config.networking.domain}" = {
      forceSSL = true;
      enableACME = true;

      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString config.services.wiki-js.settings.port}";
      };
    };
  };
}
