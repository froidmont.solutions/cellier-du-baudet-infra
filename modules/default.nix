{ config, pkgs, ... }:
{
  imports = [
    ./openssh.nix
    ./postgresql.nix
    ./nextcloud.nix
    ./wikijs.nix
    ./volunteer-management.nix
    ./website/website.nix
  ];
}
