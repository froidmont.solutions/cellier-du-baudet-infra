{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.custom.services.postgresql;
in
{
  options.custom.services.postgresql = {
    enable = mkEnableOption "postgresql";

    nextcloudDbPasswordFile = mkOption { type = types.path; };

    wikiJsDbPasswordFile = mkOption { type = types.path; };
  };

  config = mkIf cfg.enable {

    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_15;
      enableTCPIP = true;
      identMap = ''
        root_as_others         root                  postgres
        root_as_others         root                  nextcloud
        root_as_others         root                  wikijs
      '';
      authentication = ''
        local  all     postgres               peer
        local  all     all                    peer map=root_as_others
      '';
    };

    systemd.services.postgresql-setup =
      let
        pgsql = config.services.postgresql;
      in
      {
        after = [ "postgresql.service" ];
        bindsTo = [ "postgresql.service" ];
        wantedBy = [ "postgresql.service" ];
        path = [
          pgsql.package
          pkgs.util-linux
        ];
        script = ''
          set -u
          PSQL() {
              psql --port=${toString pgsql.settings.port} "$@"
          }

          PSQL -tAc "SELECT 1 FROM pg_roles WHERE rolname = 'nextcloud'" | grep -q 1 || PSQL -tAc 'CREATE ROLE "nextcloud"'
          PSQL -tAc "SELECT 1 FROM pg_roles WHERE rolname = 'wikijs'" | grep -q 1 || PSQL -tAc 'CREATE ROLE "wikijs"'

          PSQL -tAc "SELECT 1 FROM pg_database WHERE datname = 'nextcloud'" | grep -q 1 || PSQL -tAc 'CREATE DATABASE "nextcloud" OWNER "nextcloud"'
          PSQL -tAc "SELECT 1 FROM pg_database WHERE datname = 'wikijs'" | grep -q 1 || PSQL -tAc 'CREATE DATABASE "wikijs" OWNER "wikijs"'

          PSQL -tAc  "ALTER ROLE nextcloud LOGIN"
          PSQL -tAc  "ALTER ROLE wikijs LOGIN"

          nextcloud_password="$(<'${cfg.nextcloudDbPasswordFile}')"
          PSQL -tAc  "ALTER ROLE nextcloud WITH PASSWORD '$nextcloud_password'"
          wikijs_password="$(<'${cfg.wikiJsDbPasswordFile}')"
          PSQL -tAc  "ALTER ROLE wikijs WITH PASSWORD '$wikijs_password'"
        '';

        serviceConfig = {
          User = pgsql.superUser;
          Type = "oneshot";
          RemainAfterExit = true;
        };
      };

    services.postgresqlBackup = {
      enable = true;
      databases = [
        "nextcloud"
        "wikijs"
      ];
      startAt = "00:00";
    };
  };
}
