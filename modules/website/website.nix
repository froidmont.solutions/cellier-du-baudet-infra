{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.custom.services.website;
in
{
  options.custom.services.website = {
    enable = mkEnableOption "website";
  };

  config = mkIf cfg.enable {

    services.nginx = {
      virtualHosts."${config.networking.domain}" = {
        forceSSL = true;
        enableACME = true;

        locations."/" = {
          extraConfig = ''
            rewrite ^/(.*)$ https://www.${config.networking.domain}/$1 permanent;
          '';
        };
      };

      virtualHosts."www.${config.networking.domain}" = {
        forceSSL = true;
        enableACME = true;

        root = ./static;
      };
    };
  };
}
