{
  config,
  lib,
  pkgs,
  ...
}:
{
  imports = [
    ../hardware/hcloud.nix
    ../modules
  ];

  sops.secrets = {
    nextcloudDbPasswordPostgres = {
      owner = config.services.postgresql.superUser;
      key = "nextcloud/db_password";
      restartUnits = [ "postgresql-setup.service" ];
    };
    nextcloudDbPassword = {
      owner = config.users.users.nextcloud.name;
      key = "nextcloud/db_password";
      restartUnits = [ "nextcloud-setup.service" ];
    };
    nextcloudAdminPassword = {
      owner = config.users.users.nextcloud.name;
      key = "nextcloud/admin_password";
      restartUnits = [ "nextcloud-setup.service" ];
    };
    wikiJsDbPassword = {
      owner = config.services.postgresql.superUser;
      key = "wikijs/db_password";
      restartUnits = [ "postgresql-setup.service" ];
    };
    wikiJsEnvFile = {
      key = "wikijs/service_env_file";
      restartUnits = [ "wiki-js.service" ];
    };
    borgPassphrase = {
      key = "borg/passphrase";
    };
    borgSshKey = {
      key = "borg/ssh_key";
    };
    monitMailserverConfig = {
      key = "monit/mailserver_config";
      restartUnits = [ "monit.service" ];
    };
  };

  environment.systemPackages = with pkgs; [
    htop-vim
    nload
    tmux
    vim
  ];

  nix = {
    nixPath = [ "nixpkgs=${pkgs.path}" ];
  };

  services.nscd.enableNsncd = true;

  zramSwap.enable = true;

  security.acme.defaults.email = "letsencrypt@froidmont.org";
  security.acme.defaults.webroot = "/var/lib/acme/acme-challenge";
  security.acme.acceptTerms = true;

  services.nginx = {
    enable = true;

    recommendedTlsSettings = true;
    recommendedOptimisation = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;
  };

  custom = {
    services.openssh.enable = true;
    services.nextcloud = {
      enable = true;
      dbPasswordFile = config.sops.secrets.nextcloudDbPassword.path;
      adminPasswordFile = config.sops.secrets.nextcloudAdminPassword.path;
    };
    services.postgresql = {
      enable = true;
      nextcloudDbPasswordFile = config.sops.secrets.nextcloudDbPasswordPostgres.path;
      wikiJsDbPasswordFile = config.sops.secrets.wikiJsDbPassword.path;
    };
    services.wiki-js = {
      enable = true;
      envFile = config.sops.secrets.wikiJsEnvFile.path;
    };
    services.volunteer-management.enable = true;
    services.website.enable = true;
  };

  services.borgbackup.jobs.data = {
    paths = [
      "/nix/var/data"
      "/var/backup"
    ];
    doInit = true;
    repo = "borg@hel1.banditlair.com:.";
    encryption = {
      mode = "repokey-blake2";
      passCommand = "cat ${config.sops.secrets.borgPassphrase.path}";
    };
    # readWritePaths = [ "/nix/var/data/backup/" ];
    environment = {
      BORG_RSH = "ssh -i ${config.sops.secrets.borgSshKey.path}";
    };
    compression = "lz4";
    startAt = "01:00";
    prune.keep = {
      within = "2d";
      daily = 14;
      weekly = 8;
      monthly = 12;
    };
  };

  services.monit = {
    enable = true;
    config = ''
      set daemon 30
        with start delay 90

      set httpd
        port 2812
        use address 127.0.0.1
        allow localhost

      set ssl {
        verify     : enable,
      }

      include ${config.sops.secrets.monitMailserverConfig.path}


      check program failed-units with path "${pkgs.systemd}/bin/systemctl --failed"
        if content != "0 loaded units listed" then alert

      check system $HOST
        if cpu usage > 95% for 10 cycles then alert
        if memory usage > 75% for 5 times within 15 cycles then alert
        if swap usage > 50% then alert

      check filesystem root with path /
        if SPACE usage > 80% then alert

    '';
  };

  services.fail2ban.enable = true;

  networking.firewall.allowedTCPPorts = [
    80
    443
  ];
}
